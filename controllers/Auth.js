const config = require('../config/constants');
const User = require('../models/user');
const Token = require('../models/token');
const jwt = require('jsonwebtoken');
const nanoid = require('nanoid');
const bcrypt = require('bcrypt');
const AuthService = require("../services/AuthService");

// For User Login and Log out Authentication
const AuthenticationController = {

    login: (req, res) => {
        const {username, password} = req.body;
        try {
            User.findOne({username: username})
                .populate('group_id')
                .exec((err, user) => {
                    if (err || !user) {
                        res.status(400).json({
                            message: 'Error occurred, user not found',
                            error: err
                        });
                    } else {
                        bcrypt.compare(password, user.password, (err, response) => {
                            if (err || !response) {
                                res.status(401).json({
                                    message: 'FAILURE, Incorrect user password!',
                                    error: err
                                });
                            } else {
                                if (user.status === false) {
                                    res.status(401).json({
                                        message: 'Account temporarily disabled for security reasons, contact System Administrators',
                                        error: err
                                    });
                                } else {
                                    const randomNumber = nanoid();
                                    const userObject = {
                                        email: user.email,
                                        firstname: user.firstname,
                                        lastname: user.lastname,
                                        fullname: user.firstname + ' ' + user.lastname,
                                        status: user.status,
                                        id: user._id,
                                        user_group: user.group_id.name,
                                        token: randomNumber
                                    };
                                    Token.create({token: randomNumber, userId: user._id}, () => {
                                        jwt.sign(userObject, process.env.SECRET_TOKEN, {expiresIn: '7d'}, (err, token) => {
                                            res.status(201).json({
                                                message: 'Login success!',
                                                user: userObject,
                                                token: token
                                            });
                                        })

                                    });
                                }
                            }
                        });
                    }

                });


        } catch (e) {
            res.status(400).json({
                message: 'wrong password',
                error: e
            })

        }
    },

    logout: (req, res) => {
        const id = AuthService.getValue(req, 'id');
        Token.remove({userId: id}, (err) => {
            if (err) {
                res.status(400).json({
                    message: 'Error trying to logout!'
                });

            } else {
                res.status(200).json({
                    message: 'Logout successful'
                });

            }

        })
    }


};

module.exports = AuthenticationController;
