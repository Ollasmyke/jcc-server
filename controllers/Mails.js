const Mail = require('../models/mail');
const PaginateService = require("../services/PaginateService");
const MailService = require("../services/mailService");

// Mails Controller
const MailsController = {

    fetchMailsAndSend: (req, res) => {
        Mail.find({status: true}, (err, fetchedMail) => {
            if (err || !fetchedMail) {
                res.status(400).json({
                    message: 'Error Occurred fetching mails, Newsletter not sent!',
                    error: err
                });
            } else {
                const messageBody = req.body;
                let mails = [];
                for (let key in fetchedMail) {
                    mails.push(fetchedMail[key]['address']);
                }
                MailService.triggerMail(res, mails, messageBody);
            }

        })
    },

    fetchNoOfMails: (req, res) => {
        Mail.countDocuments({}, (err, totalMails) => {
            if (err || !totalMails) {
                res.status(400).json({
                    message: 'Error Occurred fetching all mails',
                    error: err
                });
            } else {
                res.status(200).json({
                    data: totalMails
                });

            }

        })
    },

    getAllMails: (req, res) => {
        const paginate = PaginateService.validatePageNo(req, res);
        Mail.countDocuments({}, (err, totalCount) => {
            if (err || !totalCount) {
                res.status(400).json({
                    message: 'Error Occurred fetching all mails',
                    error: err
                });
            } else {
                Mail.find({}, {}, paginate.query)
                    .sort({$natural: -1})
                    .exec((err, data) => {
                        if (err) {
                            res.status(400).json({
                                message: 'Error Occurred fetching all mails',
                                error: err
                            });
                        } else {
                            const pages = Math.ceil(totalCount / paginate.pageSize);
                            res.status(200).json({
                                message: 'All Mails fetched successfully',
                                pageNo: paginate.pageNo,
                                pages: pages,
                                data: data
                            });
                        }
                    });
            }
        });
    },

    createNewMail: (req, res) => {
        Mail.findOne({address: req.body.address}, (error, data) => {
            if (error) {
                res.status(400).json({
                    message: 'Error occurred while adding new mail address. Contact Server Administrator',
                    error: error,
                });
            } else {
                if (data) {
                    res.status(400).json({
                        message: 'Mail already exists'
                    });
                } else {
                    const mail = req.body.address;
                    Mail.create(mail, (err, data) => {
                        if (err) {
                            res.status(400).json({
                                message: err.errors[Object.keys(err.errors)[0]].message || 'Error occurred adding new mail address',
                                error: err,
                            })
                        } else {
                            res.status(201).json({
                                message: 'Mail added Successfully.',
                                data: data
                            });
                        }
                    })
                }

            }

        })

    },

    getMailById: (req, res) => {
        const id = req.params.id;
        Mail.findById(id, (err, data) => {
            if (err) {
                res.status(400).json({
                    message: 'Data resource unavailable, wrong ID'
                });
            } else {
                res.status(200).json({
                    message: 'Data found',
                    data: data
                });
            }
        })
    },

    toggleMail: (req, res) => {
        const id = req.params.id;
        Mail.findById(id, (err, mail) => {
            if (err) {
                res.status(400).json({
                    message: 'Data resource unavailable, wrong ID',
                    error: err
                });
            } else {
                mail.status = !mail.status;
                mail.save((err, mail) => {
                    if (err) {
                        res.status(400).json({
                            message: 'Mail status could not be updated',
                            error: err,
                        });
                    } else {
                        res.status(201).json({
                            message: 'Mail status updated successfully.',
                            mail: mail
                        });
                    }

                });
            }
        })
    },

    updateMail: (req, res) => {
        const id = req.params.id;
        Mail.findByIdAndUpdate(id, {$set: req.body}, {new: true}, (err, data) => {
            if (err) {
                res.status(400).json({
                    message: 'Data resource unavailable, wrong ID',
                    error: err
                });
            } else {
                res.status(200).json({
                    message: 'Mail Address found and updated successfully',
                    data: data
                });
            }
        })

    },

    deleteMail: (req, res) => {
        const id = req.params.id;
        Mail.findByIdAndRemove(id, (err, data) => {
            if (err) {
                res.status(400).json({
                    message: 'Data resource unavailable, wrong ID',
                    error: err,
                });
            } else {
                res.status(200).json({
                    message: 'Mail deleted successfully',
                    data: data
                });
            }
        });
    }

};

module.exports = MailsController;
