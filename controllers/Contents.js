const Content = require('../models/content');
const PaginateService = require("../services/PaginateService");

// Contents Controller
const ContentsController = {

    getAllContents: (req, res) => {
        const paginate = PaginateService.validatePageNo(req, res);
        Content.countDocuments({}, (err, totalCount) => {
            if (err || !totalCount) {
                res.status(400).json({
                    message: 'Error Occurred fetching all contents',
                    error: err
                });
            } else {
                Content.find({}, {}, paginate.query)
                    .populate('category_id')
                    .sort({$natural: -1})
                    .exec((err, data) => {
                        if (err) {
                            res.status(400).json({
                                message: 'Error Occurred fetching all contents',
                                error: err
                            });
                        } else {
                            const pages = Math.ceil(totalCount / paginate.pageSize);
                            res.status(200).json({
                                message: 'All contents fetched successfully',
                                pageNo: paginate.pageNo,
                                pages: pages,
                                data: data
                            });
                        }
                    });
            }
        });
    },

    createNewContent: (req, res) => {
        Content.findOne({name: req.body.name}, (error, data) => {
            if (error) {
                res.status(400).json({
                    message: 'Error occurred while trying to create new content. Contact Server Administrator',
                    error: error,
                });
            } else {
                if (data) {
                    res.status(400).json({
                        message: 'Content name already exists'
                    });
                } else {
                    const category = req.body;
                    Content.create(category, (err, data) => {
                        if (err) {
                            res.status(400).json({
                                message: err.errors[Object.keys(err.errors)[0]].message || 'Error occurred Creating content',
                                error: err,
                            })
                        } else {
                            res.status(201).json({
                                message: 'Content Created Successfully. Received JSON object to save',
                                user: data
                            });
                        }
                    });
                }
            }

        });
    },

    getContentById: (req, res) => {
        const id = req.params.id;
        Content.findById(id, (err, data) => {
            if (err) {
                res.status(400).json({
                    message: 'Data resource unavailable, wrong ID'
                });
            } else {
                res.status(200).json({
                    message: 'Data found',
                    data: data
                });
            }
        })
    },

    getContentByCategoryId: (req, res) => {
        const id = req.params.id;
        const paginate = PaginateService.validatePageNo(req, res);
        Content.countDocuments({}, (err, totalCount) => {
            if (err || !totalCount) {
                res.status(400).json({
                    message: 'Error Occurred fetching all contents',
                    error: err
                });
            } else {
                Content.find({category_id: id}, {}, paginate.query)
                    .sort({$natural: -1})
                    .exec((err, data) => {
                        if (err) {
                            res.status(400).json({
                                message: 'Data resource unavailable, wrong ID'
                            });
                        } else {
                            const pages = Math.ceil(totalCount / paginate.pageSize);
                            res.status(200).json({
                                message: 'Data found',
                                pageNo: paginate.pageNo,
                                pages: pages,
                                data: data
                            });
                        }
                    });
            }
        });
    },

    getActiveContentByCategoryId: (req, res) => {
        const id = req.params.id;
        const paginate = PaginateService.validatePageNo(req, res);
        Content.countDocuments({}, (err, totalCount) => {
            if (err || !totalCount) {
                res.status(400).json({
                    message: 'Error Occurred fetching all contents',
                    error: err
                });
            } else {
                Content.find({$and: [{category_id: id}, {status: true}]}, {}, paginate.query)
                    .sort({$natural: -1})
                    .exec((err, data) => {
                        if (err) {
                            res.status(400).json({
                                message: 'Data resource unavailable, wrong ID'
                            });
                        } else {
                            const pages = Math.ceil(totalCount / paginate.pageSize);
                            res.status(200).json({
                                message: 'Data found',
                                pageNo: paginate.pageNo,
                                pages: pages,
                                data: data
                            });
                        }
                    });
            }
        });

    },

    toggleContent: (req, res) => {
        const id = req.params.id;
        Content.findById(id, (err, content) => {
            if (err) {
                res.status(400).json({
                    message: 'Data resource unavailable, wrong ID',
                    error: err
                });
            } else {
                content.status = !content.status;
                content.save((err, content) => {
                    if (err) {
                        res.status(400).json({
                            message: 'Content status could not be updated',
                            error: err
                        });
                    } else {
                        res.status(201).json({
                            message: 'Content status updated successfully.',
                            content: content
                        });
                    }

                });
            }
        })
    },

    updateContent: (req, res) => {
        const id = req.params.id;
        Content.findByIdAndUpdate(id, {$set: req.body}, {new: true}, (err, data) => {
            if (err) {
                res.status(400).json({
                    message: 'Data resource unavailable, wrong ID',
                    error: err
                });
            } else {
                res.status(200).json({
                    message: 'Category data found and updated successfully',
                    data: data
                });
            }
        })

    },

    deleteContent: (req, res) => {
        const id = req.params.id;
        Content.findByIdAndRemove(id, (err, data) => {
            if (err) {
                res.status(400).json({
                    message: 'Data resource unavailable, wrong ID',
                    error: err
                });
            } else {
                res.status(200).json({
                    message: 'Category deleted successfully',
                    data: data
                });
            }
        });
    }

};

module.exports = ContentsController;
