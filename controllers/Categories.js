const Category = require('../models/category');
const PaginateService = require("../services/PaginateService");

// Categories Controller
const CategoryController = {

    getAllCategories: (req, res) => {
        const paginate = PaginateService.validatePageNo(req, res);
        Category.countDocuments({}, (err, totalCount) => {
            if (err || !totalCount) {
                res.status(400).json({
                    message: 'Error Occurred fetching all categories',
                    error: err
                });
            } else {
                Category.find({}, {}, paginate.query)
                    .sort({$natural: -1})
                    .exec((err, data) => {
                        if (err) {
                            res.status(400).json({
                                message: 'Error Occurred fetching all categories',
                                error: err
                            });
                        } else {
                            const pages = Math.ceil(totalCount / paginate.pageSize);
                            res.status(200).json({
                                message: 'All Categories fetched successfully',
                                pageNo: paginate.pageNo,
                                pages: pages,
                                data: data
                            });
                        }
                    });
            }
        });
    },

    createNewCategory: (req, res) => {
        Category.findOne({name: req.body.name}, (error, data) => {
            if (error) {
                res.status(400).json({
                    message: 'Error occurred while creating new category. Contact Server Administrator',
                    error: error,
                });
            } else {
                if (data) {
                    res.status(400).json({
                        message: 'Category name already exists'
                    });
                } else {
                    const category = req.body;
                    Category.create(category, (err, data) => {
                        if (err) {
                            res.status(400).json({
                                message: err.errors[Object.keys(err.errors)[0]].message || 'Error occurred Creating Content',
                                error: err,
                            })
                        } else {
                            res.status(201).json({
                                message: 'Category Created Successfully.',
                                user: data
                            });
                        }
                    })
                }

            }

        })

    },

    getCategoryById: (req, res) => {
        const id = req.params.id;
        Category.findById(id, (err, data) => {
            if (err) {
                res.status(400).json({
                    message: 'Data resource unavailable, wrong ID'
                });
            } else {
                res.status(200).json({
                    message: 'Data found',
                    data: data
                });
            }
        })
    },

    toggleCategory: (req, res) => {
        const id = req.params.id;
        Category.findById(id, (err, category) => {
            if (err) {
                res.status(400).json({
                    message: 'Data resource unavailable, wrong ID',
                    error: err
                });
            } else {
                category.status = !category.status;
                category.save((err, category) => {
                    if (err) {
                        res.status(400).json({
                            message: 'Category status could not be updated',
                            error: err,
                        });
                    } else {
                        res.status(201).json({
                            message: 'Category status updated successfully.',
                            category: category
                        });
                    }

                });
            }
        })
    },

    updateCategory: (req, res) => {
        const id = req.params.id;
        Category.findByIdAndUpdate(id, {$set: req.body}, {new: true}, (err, data) => {
            if (err) {
                res.status(400).json({
                    message: 'Data resource unavailable, wrong ID',
                    error: err
                });
            } else {
                res.status(200).json({
                    message: 'Category data found and updated successfully',
                    data: data
                });
            }
        })

    },

    deleteCategory: (req, res) => {
        const id = req.params.id;
        Category.findByIdAndRemove(id, (err, data) => {
            if (err) {
                res.status(400).json({
                    message: 'Data resource unavailable, wrong ID',
                    error: err,
                });
            } else {
                res.status(200).json({
                    message: 'Category deleted successfully',
                    data: data
                });
            }
        });
    }

};

module.exports = CategoryController;
