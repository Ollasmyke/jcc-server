const Group = require('../models/user_group');

// User Group Controller
const GroupController = {
    getAllGroups: (req, res) => {
        Group.find({}, (err, data) => {
            if (err) {
                res.status(400).json({
                    message: 'Error Occurred fetching all groups',
                    error: err
                });
            } else {
                res.status(200).json({
                    data: data,
                    message: 'All User Groups fetched successfully'
                });
            }
        });

    },

    createNewGroup: (req, res) => {
        // const data = req.body;
        Group.findOne({name: req.body.name}, (error, data) => {
            if (error) {
                res.status(400).json({
                    message: 'Error occurred, creating new User group. Contact Server Administrator',
                    error: error
                });
            } else {
                if (data) {
                    res.status(400).json({
                        message: 'Group name already exists'});
                } else {
                    const group = req.body;
                    Group.create(group, (err, data) => {
                        if (err) {
                            res.status(400).json({
                                message: err.errors[Object.keys(err.errors)[0]].message || 'Error occurred Creating Group',
                                error: error})
                        } else {
                            res.status(201).json({
                                message: 'Group Created Successfully. Received JSON object to save',
                                user: data});
                        }
                    })
                }
            }
        });

    },

    getGroupById: (req, res) => {
        const id = req.params.id;
        Group.findById(id, (err, data) => {
                if (err) {
                    res.status(404).json({
                        message: 'Data resource unavailable, wrong ID'});
                } else {
                    res.status(200).json({
                        message: 'Data found',
                        data: data});
                }
            })

    },

    updateGroup: (req, res) => {
        const id = req.params.id;
        Group.findByIdAndUpdate( id, {$set: req.body}, {new: true}, (err, data) => {
            if (err) {
                res.status(404).json({
                    message: 'Data resource unavailable, wrong ID',
                    error: err});
            } else {
                res.status(200).json({
                    message: 'User Group found and updated successfully',
                    data: data});
            }
        })
    },

    deleteGroup: (req, res) => {
        const id = req.params.id;
        Group.findByIdAndRemove(id, (err, data) => {
            if (err) {
                res.status(404).json({
                    message: 'Data resource unavailable, wrong ID',
                    error: err});
            } else {
                res.status(200).json({
                    message: 'Group deleted successfully',
                    data: data});
            }
        });
    }
};

module.exports = GroupController;
