{
    "swagger": "2.0",
    "info": {
        "title": "JCC Dashboard",
        "description": "JCC Server: A Mobile Application for the RCCG, Jubilee Christian Centre. The server side of the application",
        "version": "1.0.0",
        "license": {
            "name": "MIT",
            "url": "https://opensource.org/licenses/MIT"
        }
    },
    "host": "jcc-server.herokuapp.com",
    "basePath": "/apis",
    "tags": [
        {
            "name": "Users Service",
            "description": "API for users in the system"
        },
        {
            "name": "User Group",
            "description": "API for user group in the user service"
        },
        {
            "name": "Auth Service",
            "description": "API for user authentication and authorization"
        },
        {
            "name": "Category Service",
            "description": "API for content categories"
        },
        {
            "name": "Content Service",
            "description": "API for contents"
        }
    ],
    "schemes": [
        "https",
        "http",
        "ws",
        "wss"
    ],
    "consumes": [
        "application/json"
    ],
    "produces": [
        "application/json"
    ],
    "paths": {
        "/users-service/": {
            "get": {
                "tags": [
                    "Users Service"
                ],
                "description": "API Call to get All users",
                "parameters": [
                    {
                        "name": "authorization",
                        "in": "header",
                        "required": true,
                        "description": "Enter Authorization Token",
                        "type": "string"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "User list successful",
                        "schema": {
                            "$ref": "#/definitions/Users"
                        }
                    }
                }
            },
            "post": {
                "tags": [
                    "Users Service"
                ],
                "description": "API Call to create users",
                "parameters": [
                    {
                        "name": "authorization",
                        "in": "header",
                        "required": false,
                        "description": "Enter Authorization Token",
                        "type": "string"
                    },
                    {
                        "name": "Parameters",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/Users"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "User created successful",
                        "schema": {
                            "$ref": "#/definitions/Users"
                        }
                    }
                }
            },
            "patch": {
                "tags": [
                    "Users Service"
                ],
                "description": "API Call to get update user data by ID",
                "parameters": [
                    {
                        "name": "authorization",
                        "in": "header",
                        "required": false,
                        "description": "Enter Authorization Token",
                        "type": "string"
                    },
                    {
                        "name": "id",
                        "in": "path",
                        "required": true,
                        "description": "Enter Group ID",
                        "type": "string"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "User updated successful",
                        "schema": {
                            "$ref": "#/definitions/UpdateUser"
                        }
                    }
                }
            }
        },
        "/users-service/{id}": {
            "get": {
                "tags": [
                    "Users Service"
                ],
                "description": "API Call to get user by ID",
                "parameters": [
                    {
                        "name": "authorization",
                        "in": "header",
                        "required": false,
                        "description": "Enter Authorization Token",
                        "type": "string"
                    },
                    {
                        "name": "id",
                        "in": "path",
                        "required": true,
                        "description": "Enter User ID",
                        "type": "string"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "User fetched successful",
                        "schema": {
                            "$ref": "#/definitions/Users"
                        }
                    }
                }
            }
        },
        "/users-service/getUserByGroupId/{id}": {
            "get": {
                "tags": [
                    "Users Service"
                ],
                "description": "API Call to get user by Group ID",
                "parameters": [
                    {
                        "name": "authorization",
                        "in": "header",
                        "required": false,
                        "description": "Enter Authorization Token",
                        "type": "string"
                    },
                    {
                        "name": "id",
                        "in": "path",
                        "required": true,
                        "description": "Enter User ID",
                        "type": "string"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "User fetched successfully",
                        "schema": {
                            "$ref": "#/definitions/Users"
                        }
                    }
                }
            }
        },
        "/users-service/toggleUser/{id}": {
            "get": {
                "tags": [
                    "Users Service"
                ],
                "description": "API Call to get toggle user data by ID",
                "parameters": [
                    {
                        "name": "authorization",
                        "in": "header",
                        "required": false,
                        "description": "Enter Authorization Token",
                        "type": "string"
                    },
                    {
                        "name": "id",
                        "in": "path",
                        "required": true,
                        "description": "Enter Group ID",
                        "type": "string"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "User toggle successful",
                        "schema": {
                            "$ref": "#/definitions/UpdateUser"
                        }
                    }
                }
            }
        },
        "/users-service/changePassword": {
            "patch": {
                "tags": [
                    "Users Service"
                ],
                "description": "API Call to change password",
                "parameters": [
                    {
                        "name": "authorization",
                        "in": "header",
                        "required": true,
                        "description": "Enter Authorization Token",
                        "type": "string"
                    },
                    {
                        "name": "Parameters",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/UserPassword"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "User Password updated successfully",
                        "schema": {
                            "$ref": "#/definitions/UserPassword"
                        }
                    }
                }
            }
        },
        "/users-service/groups/allGroups": {
            "get": {
                "tags": [
                    "User Group"
                ],
                "description": "API Call to get Group by ID",
                "parameters": [
                    {
                        "name": "authorization",
                        "in": "header",
                        "required": false,
                        "description": "Enter Authorization Token",
                        "type": "string"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Group retrieved successfully"
                    }
                }
            }
        },
        "/users-service/groups": {
            "post": {
                "tags": [
                    "User Group"
                ],
                "description": "API Call to create a new group",
                "parameters": [
                    {
                        "name": "authorization",
                        "in": "header",
                        "required": false,
                        "description": "Enter Authorization Token",
                        "type": "string"
                    },
                    {
                        "name": "name",
                        "in": "body",
                        "required": true,
                        "description": "Enter Group Name",
                        "schema": {
                            "$ref": "#/definitions/Groups"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Group Created Successfully. Received JSON object to save"
                    }
                }
            }
        },
        "/users-service/groups/{id}": {
            "get": {
                "tags": [
                    "User Group"
                ],
                "description": "API Call to get Group by ID",
                "parameters": [
                    {
                        "name": "authorization",
                        "in": "header",
                        "required": false,
                        "description": "Enter Authorization Token",
                        "type": "string"
                    },
                    {
                        "name": "id",
                        "in": "path",
                        "required": true,
                        "description": "Enter Group ID",
                        "type": "string"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Group retrieved successfully"
                    }
                }
            },
            "patch": {
                "tags": [
                    "User Group"
                ],
                "description": "API Call to update an existing user group",
                "parameters": [
                    {
                        "name": "authorization",
                        "in": "header",
                        "required": false,
                        "description": "Enter Authorization Token",
                        "type": "string"
                    },
                    {
                        "name": "id",
                        "in": "path",
                        "required": true,
                        "description": "Enter Group ID",
                        "type": "string"
                    },
                    {
                        "name": "name",
                        "in": "body",
                        "required": true,
                        "description": "Enter Group Name",
                        "schema": {
                            "$ref": "#/definitions/Groups"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Group Updated Successfully. Received JSON object to save"
                    }
                }
            },
            "delete": {
                "tags": [
                    "User Group"
                ],
                "description": "API Call to delete an existing user group",
                "parameters": [
                    {
                        "name": "authorization",
                        "in": "header",
                        "required": false,
                        "description": "Enter Authorization Token",
                        "type": "string"
                    },
                    {
                        "name": "id",
                        "in": "path",
                        "required": true,
                        "description": "Enter Group ID",
                        "type": "string"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Group Updated Successfully. Received JSON object to save"
                    }
                }
            }
        },
        "/auth-service/login": {
            "post": {
                "tags": [
                    "Auth Service"
                ],
                "description": "Login Authentication",
                "parameters": [
                    {
                        "name": "Parameters",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/Login"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "User Login successfully",
                        "schema": {
                            "$ref": "#/definitions/Login"
                        }
                    }
                }
            }
        },
        "/auth-service/logout": {
            "get": {
                "tags": [
                    "Auth Service"
                ],
                "description": "Logout Authentication",
                "parameters": [
                    {
                        "name": "authorization",
                        "in": "header",
                        "required": true,
                        "description": "Enter Authorization Token",
                        "type": "string"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "User Logged out successfully"
                    }
                }
            }
        },
        "/content-service/category": {
            "get": {
                "tags": [
                    "Category Service"
                ],
                "description": "API Call to get All Categories",
                "parameters": [
                    {
                        "name": "authorization",
                        "in": "header",
                        "required": false,
                        "description": "Enter Authorization Token",
                        "type": "string"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Category list successful"
                    }
                }
            },
            "post": {
                "tags": [
                    "Category Service"
                ],
                "description": "API Call to get All users",
                "parameters": [
                    {
                        "name": "authorization",
                        "in": "header",
                        "required": false,
                        "description": "Enter Authorization Token",
                        "type": "string"
                    },
                    {
                        "name": "Parameters",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/Category"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "category created successful",
                        "schema": {
                            "$ref": "#/definitions/Category"
                        }
                    }
                }
            }
        },
        "/content-service/category/{id}": {
            "get": {
                "tags": [
                    "Category Service"
                ],
                "description": "API Call to get Category by ID",
                "parameters": [
                    {
                        "name": "authorization",
                        "in": "header",
                        "required": false,
                        "description": "Enter Authorization Token",
                        "type": "string"
                    },
                    {
                        "name": "id",
                        "in": "path",
                        "required": true,
                        "description": "Enter Group ID",
                        "type": "string"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Category list successful"
                    }
                }
            },
            "patch": {
                "tags": [
                    "Category Service"
                ],
                "description": "API Call to update category",
                "parameters": [
                    {
                        "name": "authorization",
                        "in": "header",
                        "required": false,
                        "description": "Enter Authorization Token",
                        "type": "string"
                    },
                    {
                        "name": "id",
                        "in": "path",
                        "required": true,
                        "description": "Enter category ID",
                        "type": "string"
                    },
                    {
                        "name": "Parameters",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/Category"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "category updated successful",
                        "schema": {
                            "$ref": "#/definitions/Category"
                        }
                    }
                }
            },
            "delete": {
                "tags": [
                    "Category Service"
                ],
                "description": "API Call to delete a category",
                "parameters": [
                    {
                        "name": "authorization",
                        "in": "header",
                        "required": false,
                        "description": "Enter Authorization Token",
                        "type": "string"
                    },
                    {
                        "name": "id",
                        "in": "path",
                        "required": true,
                        "description": "Enter category ID",
                        "type": "string"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "category deleted successfully",
                        "schema": {
                            "$ref": "#/definitions/Category"
                        }
                    }
                }
            }
        },
        "/content-service/category/toggle/{id}": {
            "get": {
                "tags": [
                    "Category Service"
                ],
                "description": "API Call to toggle category status",
                "parameters": [
                    {
                        "name": "authorization",
                        "in": "header",
                        "required": false,
                        "description": "Enter Authorization Token",
                        "type": "string"
                    },
                    {
                        "name": "id",
                        "in": "path",
                        "required": true,
                        "description": "Enter Group ID",
                        "type": "string"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Category list successful"
                    }
                }
            }
        },
        "/content-service/content": {
            "get": {
                "tags": [
                    "Content Service"
                ],
                "description": "API Call to get All Contents",
                "parameters": [
                    {
                        "name": "authorization",
                        "in": "header",
                        "required": false,
                        "description": "Enter Authorization Token",
                        "type": "string"
                    },
                    {
                        "name": "Page Number",
                        "in": "header",
                        "required": false,
                        "description": "Enter Page Number",
                        "type": "string"
                    },
                    {
                        "name": "Page Size",
                        "in": "header",
                        "required": false,
                        "description": "Enter Page Size",
                        "type": "string"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Content list successful"
                    }
                }
            },
            "post": {
                "tags": [
                    "Content Service"
                ],
                "description": "API Call to get All users",
                "parameters": [
                    {
                        "name": "authorization",
                        "in": "header",
                        "required": false,
                        "description": "Enter Authorization Token",
                        "type": "string"
                    },
                    {
                        "name": "Parameters",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/Content"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "content created successful",
                        "schema": {
                            "$ref": "#/definitions/Content"
                        }
                    }
                }
            }
        },
        "/content-service/content/{id}": {
            "get": {
                "tags": [
                    "Content Service"
                ],
                "description": "API Call to get content by ID",
                "parameters": [
                    {
                        "name": "authorization",
                        "in": "header",
                        "required": false,
                        "description": "Enter Authorization Token",
                        "type": "string"
                    },
                    {
                        "name": "id",
                        "in": "path",
                        "required": true,
                        "description": "Enter ID",
                        "type": "string"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Content list successful"
                    }
                }
            },
            "patch": {
                "tags": [
                    "Content Service"
                ],
                "description": "API Call to update content",
                "parameters": [
                    {
                        "name": "authorization",
                        "in": "header",
                        "required": false,
                        "description": "Enter Authorization Token",
                        "type": "string"
                    },
                    {
                        "name": "id",
                        "in": "path",
                        "required": true,
                        "description": "Enter ID",
                        "type": "string"
                    },
                    {
                        "name": "Parameters",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/Content"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "category updated successful",
                        "schema": {
                            "$ref": "#/definitions/Category"
                        }
                    }
                }
            },
            "delete": {
                "tags": [
                    "Content Service"
                ],
                "description": "API Call to delete a content",
                "parameters": [
                    {
                        "name": "authorization",
                        "in": "header",
                        "required": false,
                        "description": "Enter Authorization Token",
                        "type": "string"
                    },
                    {
                        "name": "id",
                        "in": "path",
                        "required": true,
                        "description": "Enter ID",
                        "type": "string"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "content deleted successfully",
                        "schema": {
                            "$ref": "#/definitions/Content"
                        }
                    }
                }
            }
        },
        "/content-service/content/toggle/{id}": {
            "get": {
                "tags": [
                    "Content Service"
                ],
                "description": "API Call to toggle content status",
                "parameters": [
                    {
                        "name": "authorization",
                        "in": "header",
                        "required": false,
                        "description": "Enter Authorization Token",
                        "type": "string"
                    },
                    {
                        "name": "id",
                        "in": "path",
                        "required": true,
                        "description": "Enter ID",
                        "type": "string"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Content list successful"
                    }
                }
            }
        },
        "/content-service/content/active/{id}": {
            "get": {
                "tags": [
                    "Content Service"
                ],
                "description": "API Call to get active content by category",
                "parameters": [
                    {
                        "name": "authorization",
                        "in": "header",
                        "required": false,
                        "description": "Enter Authorization Token",
                        "type": "string"
                    },
                    {
                        "name": "id",
                        "in": "path",
                        "required": true,
                        "description": "Enter ID",
                        "type": "string"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Content list successful"
                    }
                }
            }
        }
    },
    "definitions": {
        "Users": {
            "required": [
                "firstname",
                "lastname",
                "username",
                "password",
                "gender",
                "email",
                "group_id"
            ],
            "properties": {
                "email": {
                    "type": "string"
                },
                "firstname": {
                    "type": "string",
                    "default": "Marshall"
                },
                "lastname": {
                    "type": "string",
                    "default": "Marshall"
                },
                "username": {
                    "type": "string",
                    "default": "Marshall"
                },
                "password": {
                    "type": "string",
                    "default": "Marshall"
                },
                "gender": {
                    "type": "string",
                    "default": "Marshall"
                }
            }
        },
        "UpdateUser": {
            "required": [
                "firstname",
                "lastname",
                "username",
                "gender",
                "email",
                "group_id"
            ],
            "properties": {
                "email": {
                    "type": "string"
                },
                "firstname": {
                    "type": "string",
                    "default": "Marshall"
                },
                "lastname": {
                    "type": "string",
                    "default": "Marshall"
                },
                "username": {
                    "type": "string",
                    "default": "Marshall"
                },
                "gender": {
                    "type": "string",
                    "default": "Marshall"
                }
            }
        },
        "Groups": {
            "required": [
                "name"
            ],
            "properties": {
                "name": {
                    "type": "string"
                }
            }
        },
        "Login": {
            "required": [
                "username",
                "password"
            ],
            "properties": {
                "username": {
                    "type": "string"
                },
                "password": {
                    "type": "string"
                }
            }
        },
        "UserPassword": {
            "required": [
                "oldPassword",
                "password"
            ],
            "properties": {
                "oldPassword": {
                    "type": "string"
                },
                "password": {
                    "type": "string"
                }
            }
        },
        "Category": {
            "required": [
                "name",
                "description"
            ],
            "properties": {
                "name": {
                    "type": "string"
                },
                "description": {
                    "type": "string"
                }
            }
        },
        "Content": {
            "required": [
                "name",
                "author",
                "status"
            ],
            "properties": {
                "name": {
                    "type": "string"
                },
                "description": {
                    "type": "string"
                },
                "message": {
                    "type": "string"
                },
                "author": {
                    "type": "string"
                },
                "status": {
                    "type": "boolean"
                },
                "image": {
                    "type": "string"
                }
            }
        }
    }
}
