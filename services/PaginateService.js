// Pagination Service
const PaginateService = {

    validatePageNo: (req, res) => {
        const pageNo = parseInt(req.query['pageNo']);
        const pageSize = parseInt(req.query['pageSize']);
        if (pageNo < 0 || pageNo === 0) {
            res.status(400).json({
                message: 'Invalid Page number'
            });
        } else {
            const query = {skip: pageSize * (pageNo - 1), limit: pageSize};
            return {query: query, pageNo: pageNo, pageSize: pageSize}
        }
    }
};

module.exports = PaginateService;
