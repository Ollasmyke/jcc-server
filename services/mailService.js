const config = require('../config/constants');
const emailTemplate = require('../config/emailTemplate');
const mailJet = require('node-mailjet').connect(process.env.MAIL_JET_KEY, process.env.MAIL_JET_SECRET_TOKEN);

const sendMail = mailJet.post('send');

const mailService = {
    triggerMail: (res, mails, messageBody) => {
        mailData(res, mails, messageBody);
    }
};

module.exports = mailService;

function mailData(res, mails, messageBody) {

    const subject = messageBody['subject'],
        url = 'http://rccgjcccoventry.org',
        banner = 'http://rccgjcccoventry.org/wp-content/uploads/2019/08/RCCG-JCC-Season-of-KINGS-Banner.jpg',
        logo = 'http://rccgjcccoventry.org/wp-content/uploads/2018/11/logo-2.png',
        topic = messageBody['topic'],
        message = messageBody['message'],
        messageP2 = messageBody['messageP2'],
        signature = messageBody['signature'] || "Jubilee Christian Centre";

    let recipients = [];
    for (let email of mails) {
        recipients.push({'Email': email});
    }

    const emailData = {
        'FromEmail': 'ollasmyke@gmail.com',
        'FromName': 'Jubilee Christian Centre',
        'Subject': subject,
        'HTML-part': emailTemplate.templateProps(logo, banner, topic, message, messageP2, signature),
        'Recipients': recipients
    };

    sendMail
        .request(emailData)
        .then((result) => {
            // console.log(result.body);
            res.status(200).json({
                message: 'Message Sent Successfully'
            })
        })
        .catch((err) => {
            res.status(400).json({
                message: 'Error attempting to send Message'
            })
        });

}
