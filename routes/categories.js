const express = require('express');
const router = express.Router();
const category = require('../controllers/Categories');


/* User Group Routes listing. */
router.get('',  (req, res, next) => {
    category.getAllCategories(req, res);
});

router.post('/',  (req, res, next) => {
    category.createNewCategory(req, res);
});

router.get('/:id',  (req, res, next) => {
    category.getCategoryById(req, res);
});

router.get('/toggle/:id',  (req, res, next) => {
    category.toggleCategory(req, res);
});

router.patch('/:id',  (req, res, next) => {
    category.updateCategory(req, res);
});

router.delete('/:id', (req, res, next) => {
    category.deleteCategory(req, res)
});


module.exports = router;
