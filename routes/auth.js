const express = require('express');
const router = express.Router();
const users = require('../controllers/Users');
const auth = require('../controllers/Auth');
const authService = require('../services/AuthService');

router.post('/login', (req, res) => {
    auth.login(req, res);
});

router.get('/logout', (req, res, next) => {authService.checkAuth(req, res, next)}, (req, res) => {
    auth.logout(req, res);
});

module.exports = router;
