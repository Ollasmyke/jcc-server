const express = require('express');
const router = express.Router();
const group = require('../controllers/UserGroups');


/* User Group Routes listing. */
router.get('/allGroups',  (req, res, next) => {
    group.getAllGroups(req, res);
});

router.post('/',  (req, res, next) => {
    group.createNewGroup(req, res);
});

router.get('/:id',  (req, res, next) => {
    group.getGroupById(req, res);
});

router.patch('/:id',  (req, res, next) => {
    group.updateGroup(req, res);
});

router.delete('/:id', (req, res, next) => {
    group.deleteGroup(req, res)
});


module.exports = router;
