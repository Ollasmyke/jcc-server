const express = require('express');
const router = express.Router();
const mail = require('../controllers/Mails');

/* Mails Routes listing. */
router.get('',  (req, res, next) => {
    mail.getAllMails(req, res);
});

router.get('/noOfMails',  (req, res, next) => {
    mail.fetchNoOfMails(req, res);
});

router.post('/sendmail',  (req, res, next) => {
    mail.fetchMailsAndSend(req, res);
});

router.post('/',  (req, res, next) => {
    mail.createNewMail(req, res);
});

router.get('/:id',  (req, res, next) => {
    mail.getMailById(req, res);
});

router.get('/toggle/:id',  (req, res, next) => {
    mail.toggleMail(req, res);
});

router.patch('/:id',  (req, res, next) => {
    mail.updateMail(req, res);
});

router.delete('/:id', (req, res, next) => {
    mail.deleteMail(req, res)
});


module.exports = router;
