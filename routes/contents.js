const express = require('express');
const router = express.Router();
const content = require('../controllers/Contents');


/* User Group Routes listing. */
router.get('',  (req, res, next) => {
    content.getAllContents(req, res);
});

router.post('/',  (req, res, next) => {
    content.createNewContent(req, res);
});

router.get('/:id',  (req, res, next) => {
    content.getContentById(req, res);
});

router.get('/category/:id',  (req, res, next) => {
    content.getContentByCategoryId(req, res);
});

router.get('/active/:id',  (req, res, next) => {
    content.getActiveContentByCategoryId(req, res);
});

router.get('/toggle/:id',  (req, res, next) => {
    content.toggleContent(req, res);
});

router.patch('/:id',  (req, res, next) => {
    content.updateContent(req, res);
});

router.delete('/:id', (req, res, next) => {
    content.deleteContent(req, res)
});


module.exports = router;
