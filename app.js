require('dotenv').config();
const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const logger = require('morgan');

const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');
const config = require('./config/constants');

// Connection begins
mongoose.connect(process.env.MONGO_ATLAS_URL, config.MONGO_OPTIONS);
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'Connection error to MongoDB Server. Check if Server is running'));
db.once('open', () => {
  console.log('Connected correctly to the MongoDB local server JCC Mobile App Server..');
});
// Connection ends here.Connection error to MYSQL DB Server. Check if Server is running

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const userGroupsRouter = require('./routes/userGroups');
const authenticationRouter = require('./routes/auth');
const categoryRouter = require('./routes/categories');
const contentRouter = require('./routes/contents');
const mailRouter = require('./routes/mails');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Allow CORS Origin for Access Control between Backend and Frontend
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, PATCH, DELETE, OPTIONS');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  res.header("Accept", "application/json");
  res.header('Access-Control-Allow-Credentials', true);
  next();
});

// Routes URIs to APIs..
app.use('/apis/discovery-service', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use('/apis/test', indexRouter);
app.use('/apis/users-service/', usersRouter);
app.use('/apis/users-service/groups', userGroupsRouter);
app.use('/apis/auth-service/', authenticationRouter);
app.use('/apis/content-service/category', categoryRouter);
app.use('/apis/content-service/content', contentRouter);
app.use('/apis/newsletter-service', mailRouter);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// error handler
app.use((err, req, res, next) =>  {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
