This Project, JCC Server: RCCG JCC Server: A Mobile Application for the RCCG, Jubilee Christian Centre. The server side of the application<br>

Developed by ollasmyke@gmail.com.

- Version 1.0 - 26th August, 2019.

The project was auto created by Express Generator, for the Server side.
MongoDB for the Database connection.

In the project directory, you can run:
### `npm start`
Runs the app in the development mode.<br>
Open [http://localhost:8004](http://localhost:8004) to view it in the browser.<br/>
Swagger UI for API documentation.
### `npm test`
Runs the test and code coverage in the development mode.<br>

Git Flow Workflow as Branching Model

This is the Back End version for this App.
