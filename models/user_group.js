const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const Schema = mongoose.Schema;

// User group schema
const userGroupSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true,
        uniqueCaseInsensitive: true,
        dropDups: true,
        trim: true,
    },
    permissions: {
        type: Object,
        default: {}
    }
}, {
    timestamps: true
});


userGroupSchema.plugin(uniqueValidator);

const userGroups = mongoose.model('Group', userGroupSchema);

module.exports = userGroups;
