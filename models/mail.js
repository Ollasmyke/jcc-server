const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const Schema = mongoose.Schema;

// Newsletter Mail schema
const mailSchema = new Schema({
    address: {
        type: String,
        required: true,
        unique: true,
        uniqueCaseInsensitive: true,
        dropDups: true,
        trim: true,
    },
    status: {
        type: String,
        required: true,
        default: false
    }
}, {
    timestamps: true
});


mailSchema.plugin(uniqueValidator);

const mail = mongoose.model('Mails', mailSchema);

module.exports = mail;
