const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const Schema = mongoose.Schema;

// Content schema
const contentSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true,
        uniqueCaseInsensitive: true,
        dropDups: true,
        trim: true,
    },
    description: {
        type: String
    },
    message: {
        type: String,
    },
    status: {
        type: Boolean,
        required: true,
        default: false
    },
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Users',
        required: true
    },
    author: {
        type: String,
        required: true
    },
    category_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Categories',
        required: true
    },
    image: {
        type: String,
    }
}, {
    timestamps: true
});


contentSchema.plugin(uniqueValidator);

const content = mongoose.model('Contents', contentSchema);

module.exports = content;
