const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const Schema = mongoose.Schema;

// Content category schema
const categorySchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true,
        uniqueCaseInsensitive: true,
        dropDups: true,
        trim: true,
    },
    description: {
        type: String
    },
    status: {
        type: Boolean,
        required: true,
        default: false
    }
}, {
    timestamps: true
});


categorySchema.plugin(uniqueValidator);

const category = mongoose.model('Categories', categorySchema);

module.exports = category;
